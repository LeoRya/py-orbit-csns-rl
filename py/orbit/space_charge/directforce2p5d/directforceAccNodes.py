"""
Module. Includes classes for all 2.5D space charge accelerator nodes.
"""

import sys
import os
import math

# import the function that finalizes the execution
from orbit.utils import orbitFinalize

# import general accelerator elements and lattice
from orbit.lattice import AccLattice, AccNode, AccActionsContainer, AccNodeBunchTracker

#import the base DirectForce AccNode class
from orbit.space_charge.scAccNodes import SC_Base_AccNode

class DirectForce2p5D_AccNode(SC_Base_AccNode):
	"""
	The subclass of the AccNodeBunchTracker class. It uses SpaceChargeCalc2p5D wrapper for the c++ space charge calculator.
	"""
	def __init__(self, sc_calculator, name = "no name"):			
		"""
		Constructor. Creates the 2p5 SC accelerator node element.
		"""
		SC_Base_AccNode.__init__(self, sc_calculator, name)
		self.setType("DirectForce2p5D")
		
	def track(self, paramsDict):
		"""
		It is tracking the bunch through the Space Charge calculator.
		"""

		if(self.switcher != True): return
		bunch = paramsDict["bunch"]
		self.sc_calculator.trackBunch(bunch,self.sc_length)

class DirectForce2p5DTEST_AccNode(SC_Base_AccNode):
	"""
	The subclass of the AccNodeBunchTracker class. It uses SpaceChargeCalc2p5D wrapper for the c++ space charge calculator.
	"""
	def __init__(self, sc_calculator, name = "no name"):			
		"""
		Constructor. Creates the 2p5 SC accelerator node element.
		"""
		SC_Base_AccNode.__init__(self, sc_calculator, name)
		self.setType("DirectForce2p5D")
		
	def track(self, paramsDict):
		"""
		It is tracking the bunch through the Space Charge calculator.
		"""

		if(self.switcher != True): return
		bunch = paramsDict["bunch"]
		syncPart = bunch.getSyncParticle()
		syncTime = syncPart.time()
		timeList = paramsDict["bucketLength"][0]
		n_tuple = len(timeList) - 1
		bucketLengthList = paramsDict["bucketLength"][1]
		bucketLength = interp(syncTime, n_tuple, timeList, bucketLengthList) #get bunchLenth by linear interpolation
		self.sc_calculator.trackBunch(bunch,self.sc_length,bucketLength)


def interp(x, n_tuple, x_tuple, y_tuple):
	"""
	Linear interpolation: Given n-tuple + 1 points,
	x_tuple and y_tuple, routine finds y = y_tuple
	at x in x_tuple. Assumes x_tuple is increasing array.
	"""
	if x <= x_tuple[0]:
		y = y_tuple[0]
		return y
	if x >= x_tuple[n_tuple]:
		y = y_tuple[n_tuple]
		return y
	dxp = x - x_tuple[0]
	for n in range(n_tuple):
		dxm = dxp
		dxp = x - x_tuple[n + 1]
		dxmp = dxm * dxp
		if dxmp <= 0:
			break
	y = (-dxp * y_tuple[n] + dxm * y_tuple[n + 1]) /\
		(dxm - dxp)
	return y		



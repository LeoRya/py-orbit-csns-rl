"""
This module is a foil node class for TEAPOT lattice
"""

import os
import math
import orbit_mpi
from orbit.utils import orbitFinalize, NamedObject, ParamsDictObject
from orbit.lattice import AccNode, AccActionsContainer, AccNodeBunchTracker
from orbit.teapot import DriftTEAPOT
from foil import Foil

class TeapotFoilNode(DriftTEAPOT):
	"""
	The foil node class for TEAPOT lattice
	"""
	def __init__(self, xmin, xmax, ymin, ymax, thick, name = "foil no name"):
		"""
		Constructor. Creates the Foil TEAPOT element.
		"""
		DriftTEAPOT.__init__(self,name)
		self.foil = Foil(xmin, xmax, ymin, ymax, thick)
		self.setType("foil teapot")
		self.setLength(0.0)
		# The user choice of scattering routine. Defualt (0) is full scatter
		self.scatterChoice = 0

	def track(self, paramsDict):
		"""
		The foil-teapot class implementation of the AccNodeBunchTracker class track(probe) method.
		"""
		length = self.getLength(self.getActivePartIndex())
		bunch = paramsDict["bunch"]
		lostbunch = paramsDict["lostbunch"]
		if(self.scatterChoice == 0):
			self.foil.traverseFoilFullScatter(bunch, lostbunch)
		else:
			self.foil.traverseFoilSimpleScatter(bunch)
	
	
	def setScatterChoice(self, choice):
		self.scatterChoice = choice
		

class TeapotSimpleFoilNode(DriftTEAPOT):
    """The simple foil node is only used to mark the proton particles crossing the foil"""

    def __init__(self, xmin, xmax, ymin, ymax, filename, name="simple foil"):
        """Constructor"""
        DriftTEAPOT.__init__(self, name)
        self.setLength(0.0)
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.cross_number = 0
        self.file_out = open(filename, 'a')
        self.columns = ['turn', 'cross-number', 'remain particle']
        self.title = 0

    def track(self, paramsDict):
        """track method"""

        comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
        rank = orbit_mpi.MPI_Comm_rank(comm)
        bunch = paramsDict["bunch"]
        size = bunch.getSize()
        if bunch.hasBunchAttrInt("Turn"):
            turn = bunch.bunchAttrInt("Turn")
        else:
            orbitFinalize("The turn attribute is needed to mark the cross number!")
        for i in xrange(size):
            x = bunch.x(i)
            y = bunch.y(i)
            if((x > self.xmin) & (x < self.xmax) & (y > self.ymin) & (y < self.ymax)):
                self.cross_number += 1

        commsize = orbit_mpi.MPI_Comm_size(comm)
        op = orbit_mpi.mpi_op.MPI_SUM
        data_type = orbit_mpi.mpi_datatype.MPI_INT
        cross_sum = orbit_mpi.MPI_Allreduce(self.cross_number, data_type, op, comm)
        global_size = bunch.getSizeGlobal()
        if rank==0:
            values = [turn, cross_sum, global_size]
            if not self.title:
                columns = '#' + '\t'.join(self.columns) + "\n"
                self.file_out.write(columns)
                self.title = 1
            contents = '\t'.join(map(str, values)) + "\n"
            self.file_out.write(contents)

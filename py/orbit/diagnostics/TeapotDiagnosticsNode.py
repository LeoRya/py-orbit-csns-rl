"""
This module is a diagnostic node class for TEAPOT lattice
"""

import os
import math
import orbit_mpi
import numpy as np

# import the auxiliary classes
from scipy.optimize import curve_fit
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from matplotlib.colors import LogNorm

from bunch import Bunch
from orbit.utils import orbitFinalize, NamedObject, ParamsDictObject

# import general accelerator elements and lattice
from orbit.lattice import AccNode, AccActionsContainer, AccNodeBunchTracker

# import Diagnostics classes
from diagnostics import StatLats, StatLatsSetMember, WCM
from diagnostics import Moments, MomentsSetMember, BPMSignal

# import teapot drift class
from orbit.teapot import DriftTEAPOT

#import Bunch diagnostics
from bunch import BunchTuneAnalysis
from bunch import BunchTwissAnalysis
from orbit.time_dep.time_dep_matrix_lattice import TIME_DEP_MATRIX_Lattice

class TeapotStatLatsNode(DriftTEAPOT):
    """
    The statlats node class for TEAPOT lattice
    """
    def __init__(self, filename , name = "statlats no name"):
        """
        Constructor. Creates the StatLats TEAPOT element.
        """
        DriftTEAPOT.__init__(self,name)
        self.statlats = StatLats(filename)
        self.setType("statlats teapot")
        self.setLength(0.0)
        self.position = 0.0
        self.lattlength = 0.0

    def track(self, paramsDict):
        """
        The statlats-teapot class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        length = self.getLength(self.getActivePartIndex())
        bunch = paramsDict["bunch"]
        self.statlats.writeStatLats(self.position,bunch,self.lattlength)

    def setPosition(self,pos):
        self.position = pos

    def closeFile(self):
        self.statlats.closeStatLats()

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength

class TeapotStatLatsNodeSetMember(DriftTEAPOT):
    """
            The statlats node class for TEAPOT lattice
    """
    def __init__(self, file, name = "statlats no name"):
        """
                Constructor. Creates the StatLats TEAPOT element.
        """
        DriftTEAPOT.__init__(self,name)
        self.statlats = StatLatsSetMember(file)
        self.setType("statlats teapot")
        self.setLength(0.0)
        self.position = 0.0
        self.lattlength = 0.0
        self.active = True
        self.file = file

    def track(self, paramsDict):
        if(self.active):
            length = self.getLength(self.getActivePartIndex())
            bunch = paramsDict["bunch"]
            self.statlats.writeStatLats(self.position,bunch,self.lattlength)

    def setPosition(self,pos):
        self.position = pos

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength

    def activate(self):
        self.active = True

    def deactivate(self):
        self.active = False

    def resetFile(self, file):
        self.file = file
        self.statlats.resetFile(self.file)


class TeapotMomentsNode(DriftTEAPOT):
    """
    The moments node class for TEAPOT lattice
    """
    def __init__(self, filename, order, nodispersion = True, emitnorm = False, name = "moments no name"):
        """
        Constructor. Creates the StatLats TEAPOT element.
        """
        DriftTEAPOT.__init__(self,name)
        self.moments = Moments(filename, order, nodispersion, emitnorm)
        self.setType("moments teapot")
        self.setLength(0.0)
        self.position = 0.0
        self.lattlength = 0.0
        self.file_out = open(filename,"w")

    def track(self, paramsDict):
        """
        The moments-teapot class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        length = self.getLength(self.getActivePartIndex())
        bunch = paramsDict["bunch"]
        self.moments.writeMoments(self.position,bunch,self.lattlength)

    def setPosition(self,pos):
        self.position = pos

    def closeMoments(self):
        self.file_out.close()

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength


class TeapotMomentsNodeSetMember(DriftTEAPOT):
    """
    The moments node class for TEAPOT lattice
    """
    def __init__(self, file, order, nodispersion = True, emitnorm = False, name = "moments no name"):
        """
        Constructor. Creates the Moments TEAPOT element.
        """
        DriftTEAPOT.__init__(self,str(name))
        self.file = file
        self.moments = MomentsSetMember(self.file, order, nodispersion, emitnorm)
        self.setType("moments teapot")
        self.setLength(0.0)
        self.position = 0.0
        self.lattlength = 0.0
        self.active = True

    def track(self, paramsDict):
        """
        The moments-teapot class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        if(self.active):
            length = self.getLength(self.getActivePartIndex())
            bunch = paramsDict["bunch"]
            self.moments.writeMoments(self.position, bunch, self.lattlength)

    def setPosition(self,pos):
        self.position = pos

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength

    def activate(self):
        self.active = True

    def deactivate(self):
        self.active = False

    def resetFile(self, file):
        self.file = file
        self.moments.resetFile(self.file)

class TeapotTuneSpreadNode(DriftTEAPOT):

    def __init__(self, name = "tune spread", point = 10, fileName = None):
        '''
        Constructor. Creates the Tune spread TEAPOT element
        '''
        DriftTEAPOT.__init__(self, name)
        self.setType("Tune spread calculator teapot")
        self.setLength(0.0)
        self.bunches = []
        self.dpp_c = []
        self.sampleSize = 10 #default sample size
        self.setCalcPoint(point)
        self.thruput = 10000
        self.active = False
        self.betax = 0
        self.alphax = 0
        self.etax = 0
        #self.etapx = 0
        self.betay = 0
        self.alphay = 0
        self.tunex = 0#the initial horizontal tune
        self.tuney = 0#the initial vertical tune
        if fileName:
            self.file_out = open(fileName, 'a')
        else:
            self.file_out = None

    def track(self, paramsDict):
        bunch = paramsDict["bunch"]
        turn = bunch.bunchAttrInt("Turn")
        if self.active:
            if turn in self.sampleTurns:
                sample_bunch = Bunch()
                self.__sampling(bunch, sample_bunch)
                self.bunches.append(sample_bunch)
            if turn == self.calcPoint:
                #do the calculation
                self.__doCalculation(bunch)

    def __doCalculation(self, bunch):
        def tune_fit(x, a, b, w, c):
            return a*np.cos(x*2*w*np.pi)+b*np.sin(x*2*w*np.pi)+c
        print("calculating the tune spread!")
        xtune = []
        ytune = []
        bsize = bunch.getSize()
        for i in xrange(bsize):
            datax = []
            datay = []
            for j in xrange(self.sampleSize):
                x = self.bunches[j].x(i)
                y = self.bunches[j].y(i)
                de = self.bunches[j].dE(i)
                dpp = self.dpp_c[j]*de
                x = x-self.etax*dpp #eliminate the effect of energy spread
                datax.append(x)
                datay.append(y)
            turns = np.arange(0, self.sampleSize, 1)
            datax = np.array(datax)
            datay = np.array(datay)
            fitax, fitbx = curve_fit(tune_fit, turns, datax, [1,self.tunex,1,0])
            fitay, fitby = curve_fit(tune_fit, turns, datay, [1,self.tuney,1,0])
            tx = math.modf(abs(fitax[1]))[0]
            ty = math.modf(abs(fitay[1]))[0]
            if tx < 0.5:
                tx = 1-tx
            if ty < 0.5:
                ty = 1-ty
            xtune.append([tx, np.diag(fitbx)[1]])
            ytune.append([ty, np.diag(fitby)[1]])
        for k in xrange(len(xtune)):
            const = "%- .8f  %- .8g  %- .8f  %- .8g\n" % (xtune[k][0],
                    xtune[k][1], ytune[k][0], ytune[k][1])
            self.file_out.write(const)


    def __sampling(self, mainBunch, newBunch):
        '''sample some particles from main bunch'''
        #mainBunch.copyEmptyBunchTo(newBunch)
        #for the moment we cache all the particles
        mainBunch.copyBunchTo(newBunch)
        syncPart = mainBunch.getSyncParticle()
        beta = syncPart.beta()
        Etot = syncPart.kinEnergy() + syncPart.mass()
        c = 1/(beta*beta*Etot)
        self.dpp_c.append(c)

    def assignTwiss(self, betax, alphax, etax, betay, alphay):
        self.betax = betax
        self.alphax = alphax
        self.etax = etax
        #self.etapx = etapx
        self.betay = betay
        self.alphay = alphay

    def assignTune(self, tunex, tuney):
        self.tunex = tunex
        self.tuney = tuney

    def setSampleSize(self, size):
        '''set the sample size'''
        self.sampleSize = size
        self.setCalcPoint(self.calcPoint)

    def setCalcPoint(self, point):
        self.calcPoint = point
        start = self.calcPoint - self.sampleSize + 1
        end = self.calcPoint + 1
        self.sampleTurns = list(xrange(start, end))

    def setThruput(self, thruput):
        '''set the thruput'''
        self.thruput = thruput

    def setFileName(self, fileName):
        self.file_out = open(fileName, 'a')

    def closeFile(self):
        self.file_out.close()

    def activate(self):
        self.active = True

    def deactivate(self):
        self.active = False

class TeapotTuneMachineNode(DriftTEAPOT):

    def __init__(self, lattice, fileName, name = "tune no name"):
        """
        Constructor. Creates the Tune TEAPOT element.
        """
        DriftTEAPOT.__init__(self,name)
        self.setType("tune calculator teapot")
        self.setLength(0.0)
        self.lattice = lattice
        self.b_tune = Bunch()
        self.b_tune.getSyncParticle().kinEnergy(0.08)
        self.matrix = TIME_DEP_MATRIX_Lattice(self.lattice, self.b_tune)
        self.file_out = open(fileName, 'a')

    def track(self, paramsDict):
        """
        The tunemachine node calculates the tune of the machine.
        """
        comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
        rank = orbit_mpi.MPI_Comm_rank(comm)
        if rank == 0:
            bunch = paramsDict["bunch"]
            syncP = bunch.getSyncParticle()
            t = syncP.time()
            syn_tune = self.b_tune.getSyncParticle()
            syn_tune.time(t)
            syn_tune.kinEnergy(syncP.kinEnergy())
            self.matrix.rebuild(self.b_tune)
            ring_par_dict = self.matrix.getRingParametersDict()
            tx = ring_par_dict["fractional tune x"]
            ty = ring_par_dict["fractional tune y"]
            if 0 < tx < 0.5:
                tx = 1-tx
            if tx < 0:
                tx = 1+tx
            if 0 < ty < 0.5:
                ty = 1-ty
            if ty < 0:
                ty = 1+ty
            const = "%- .8f %- .8f  %- .8f\n" % (t, tx, ty)
            self.file_out.write(const)

class TeapotTuneAnalysisNode(DriftTEAPOT):

    def __init__(self, name = "tuneanalysis no name"):
        """
        Constructor. Creates the Tune TEAPOT element.
        """
        DriftTEAPOT.__init__(self,name)
        self.bunchtune = BunchTuneAnalysis()
        self.setType("tune calculator teapot")
        self.lattlength = 0.0
        self.setLength(0.0)
        self.position = 0.0

    def track(self, paramsDict):
        """
        The bunchtuneanalysis-teapot class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        length = self.getLength(self.getActivePartIndex())
        bunch = paramsDict["bunch"]
        self.bunchtune.analyzeBunch(bunch)

    def setPosition(self,pos):
        self.position = pos

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength

    def assignTwiss(self, betax, alphax, etax, etapx, betay, alphay):
        self.bunchtune.assignTwiss(betax, alphax, etax, etapx, betay, alphay)

class TeapotBPMSignalNode(DriftTEAPOT):

    def __init__(self, name = "BPMSignal no name"):
        """
        Constructor. Creates the BPM TEAPOT element.
        """
        DriftTEAPOT.__init__(self,name)
        self.bpm = BPMSignal()
        self.setType("BPMSignal")
        self.lattlength = 0.0
        self.setLength(0.0)
        self.position = 0.0

    def track(self, paramsDict):
        """
        The BPM class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        length = self.getLength(self.getActivePartIndex())
        bunch = paramsDict["bunch"]
        self.bpm.analyzeSignal(bunch)

    def setPosition(self,pos):
        self.position = pos

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength

    def getSignal(self):
        xAvg = self.bpm.getSignalX()
        yAvg = self.bpm.getSignalY()
        return xAvg, yAvg

class TeapotBPMTBTNode(DriftTEAPOT):

    def __init__(self, name = "BPMSignal no name", filename = None):
        """
        Constructor. Creates the StatLats TEAPOT element.
        """
        DriftTEAPOT.__init__(self,name)
        self.bpm = BPMSignal()
        self.setType("BPMTBTSignal")
        self.lattlength = 0.0
        self.setLength(0.0)
        self.position = 0.0
        if filename:
            self.file_out = open(filename,'a')
        else:
            self.file_out = None
        self.active = False

    def track(self, paramsDict):
        """
        The bunchtuneanalysis-teapot class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        if self.active and self.file_out:
            length = self.getLength(self.getActivePartIndex())
            bunch = paramsDict["bunch"]
            self.bpm.analyzeSignal(bunch)
            xAvg = self.bpm.getSignalX()
            yAvg = self.bpm.getSignalY()
            comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
            rank = orbit_mpi.MPI_Comm_rank(comm)
            sp = bunch.getSyncParticle()
            time = sp.time()
            turn = bunch.bunchAttrInt("Turn")
            if rank==0:
                cont = "%- .8g  %05d  %- .8f  %- .8f\n" %(time, turn, xAvg, yAvg)
                self.file_out.write(cont)

    def setPosition(self,pos):
        self.position = pos

    def setFileName(self, filename):
        self.file_out = open(filename, 'a')

    def closeFile(self):
        self.file_out.close()

    def activate(self):
        self.active = True

    def deactivate(self):
        self.active = False

    def getSignal(self):
        '''Get the current bpm signal'''
        xAvg = self.bpm.getSignalX()
        yAvg = self.bpm.getSignalY()
        return xAvg, yAvg

class TeapotWCMNode(DriftTEAPOT):

    def __init__(self, name = "WCM"):
        """
        Constructor. Creates the WCM TEAPOT element.
        """
        DriftTEAPOT.__init__(self,name)
        self.dector = WCM()
        self.setType("WCM")
        self.setLength(0.0)
        self.lattlength = 0.0
        self.position = 0.0
        self.active = True

    def track(self, paramsDict):
        """
        The WCM class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        if self.active:
            b = paramsDict["bunch"]
            self.dector.extractCoordinates(b)
            self.active = False

    def setPosition(self,pos):
        self.position = pos

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength

    def activate(self):
        self.active = True

    def deactivate(self):
        self.active = False

    def getSignal(self):
        return self.dector.getCoordinates()

class TeapotDumpNode(DriftTEAPOT):

    def __init__(self, filename, interval, turn, dumpFile = True, plotFile = True, name = "dump"):
        """
        Constructor. Creates the dump TEAPOT element.
        dump the bunch to file in specified position,this node need TeapotCounterNode.
        There should be a TeapotCounterNode in the lattice
        """
        DriftTEAPOT.__init__(self,name)
        self.setType("dump")
        self.lattlength = 0.0
        self.position = 0.0
        self.setLength(0.0)
        self.filename = filename
        self.interval = interval
        self.start = 0
        self.end = float("inf")
        self.totalturn = turn #Total tracking turn
        self.dumpFile = dumpFile
        self.plotFile = plotFile
        self.bunchtwissanalysis = BunchTwissAnalysis()

    def track(self, paramsDict):
        """
        The dump class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        bunch = paramsDict["bunch"]
        turn = bunch.bunchAttrInt("Turn")
        if (turn >= self.start and turn <= self.end) and turn%self.interval==0:
            self.bunchtwissanalysis.analyzeBunch(bunch)
            emitx = self.bunchtwissanalysis.getEmittance(0)
            noremitx = self.bunchtwissanalysis.getEmittanceNormalized(0)
            effemitx = self.bunchtwissanalysis.getEffectiveEmittance(0)
            betax = self.bunchtwissanalysis.getBeta(0)
            effbetax = self.bunchtwissanalysis.getEffectiveBeta(0)
            alphax = self.bunchtwissanalysis.getAlpha(0)
            effalphax = self.bunchtwissanalysis.getEffectiveAlpha(0)
            betay = self.bunchtwissanalysis.getBeta(1)
            effbetay = self.bunchtwissanalysis.getEffectiveBeta(1)
            alphay = self.bunchtwissanalysis.getAlpha(1)
            effalphay = self.bunchtwissanalysis.getEffectiveAlpha(1)
            emity = self.bunchtwissanalysis.getEmittance(1)
            noremity = self.bunchtwissanalysis.getEmittanceNormalized(1)
            effemity = self.bunchtwissanalysis.getEffectiveEmittance(1)
            dispersionx = self.bunchtwissanalysis.getDispersion(0)
            dispersiony = self.bunchtwissanalysis.getDispersion(1)

            bunch.bunchAttrDouble("dumpPosition", self.position)
            bunch.bunchAttrDouble("EmittanceX",emitx)
            bunch.bunchAttrDouble("EmittanceY",emity)
            bunch.bunchAttrDouble("NormalizedEmittanceX",noremitx)
            bunch.bunchAttrDouble("NormalizedEmittanceY",noremity)
            bunch.bunchAttrDouble("EffectiveEmittanceX",effemitx)
            bunch.bunchAttrDouble("EffectiveEmittanceY",effemity)
            bunch.bunchAttrDouble("BetaX",betax)
            bunch.bunchAttrDouble("BetaY",betay)
            bunch.bunchAttrDouble("AlphaX",alphax)
            bunch.bunchAttrDouble("AlphaY",alphay)
            bunch.bunchAttrDouble("EffectiveBetaX",effbetax)
            bunch.bunchAttrDouble("EffectiveBetaY",effbetay)
            bunch.bunchAttrDouble("EffectiveAlphaX",effalphax)
            bunch.bunchAttrDouble("EffectiveAlphaY",effalphay)
            bunch.bunchAttrDouble("DispersionX",dispersionx)
            bunch.bunchAttrDouble("DispersionY",dispersiony)
            namearr = self.filename.split('/')
            namearr[-1] = str(turn)+"_"+namearr[-1]
            filename = "/".join(namearr)
            if self.dumpFile:
                bunch.dumpBunch(filename + '.bunch')
            if self.plotFile:
                self.plotBunch(bunch,filename + '.png')

        if int(turn) == min(self.end, self.totalturn):
            namearr = self.filename.split('/')
            namearr[-1] = str(turn)+"_"+namearr[-1]
            namearr[-1] = "lost"+"_"+namearr[-1] + '.bunch'
            lostfile = "/".join(namearr)
            lostbunch = paramsDict["lostbunch"]
            lostbunch.bunchAttrInt("Turn", turn)
            lostbunch.bunchAttrDouble("dumpPosition", self.position)
            lostbunch.dumpBunch(lostfile)

    def plotBunch(self, bunch, filename):
        comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
        rank = orbit_mpi.MPI_Comm_rank(comm)
        index = 0
        macroPart = bunch.getSize()

        coords = [[], [], [], [], [], []]
        while index < macroPart:
            coords[0].append(bunch.x(index))
            coords[1].append(bunch.px(index))
            coords[2].append(bunch.y(index))
            coords[3].append(bunch.py(index))
            coords[4].append(bunch.z(index))
            coords[5].append(bunch.pz(index))
            index += 1

        commsize = orbit_mpi.MPI_Comm_size(comm)

        if rank != 0:
            orbit_mpi.MPI_Send(coords[0],
                    orbit_mpi.mpi_datatype.MPI_DOUBLE,0,100,comm)
            orbit_mpi.MPI_Send(coords[1],
                    orbit_mpi.mpi_datatype.MPI_DOUBLE,0,101,comm)
            orbit_mpi.MPI_Send(coords[2],
                    orbit_mpi.mpi_datatype.MPI_DOUBLE,0,102,comm)
            orbit_mpi.MPI_Send(coords[3],
                    orbit_mpi.mpi_datatype.MPI_DOUBLE,0,103,comm)
            orbit_mpi.MPI_Send(coords[4],
                    orbit_mpi.mpi_datatype.MPI_DOUBLE,0,104,comm)
            orbit_mpi.MPI_Send(coords[5],
                    orbit_mpi.mpi_datatype.MPI_DOUBLE,0,105,comm)

        else:
            coordsSum = coords
            for source in xrange(commsize-1):
                x = orbit_mpi.MPI_Recv(orbit_mpi.mpi_datatype.MPI_DOUBLE,
                        source+1, 100, comm)
                coordsSum[0] = coordsSum[0] + list(x)
                xp = orbit_mpi.MPI_Recv(orbit_mpi.mpi_datatype.MPI_DOUBLE,
                        source+1, 101, comm)
                coordsSum[1] = coordsSum[1] + list(xp)
                y = orbit_mpi.MPI_Recv(orbit_mpi.mpi_datatype.MPI_DOUBLE,
                        source+1, 102, comm)
                coordsSum[2] = coordsSum[2] + list(y)
                yp = orbit_mpi.MPI_Recv(orbit_mpi.mpi_datatype.MPI_DOUBLE,
                        source+1, 103, comm)
                coordsSum[3] = coordsSum[3] + list(yp)
                z = orbit_mpi.MPI_Recv(orbit_mpi.mpi_datatype.MPI_DOUBLE,
                        source+1, 104, comm)
                coordsSum[4] = coordsSum[4] + list(z)
                dE = orbit_mpi.MPI_Recv(orbit_mpi.mpi_datatype.MPI_DOUBLE,
                        source+1, 105, comm)
                coordsSum[5] = coordsSum[5] + list(dE)
        if rank == 0:
            turn = bunch.bunchAttrInt("Turn")
            plt.subplot(421)
            plt.hist2d(coordsSum[4], coordsSum[5], bins = 256, norm = LogNorm())
            plt.xlabel('z(m)')
            plt.ylabel('dE(GeV)')

            plt.subplot(422)
            plt.hist2d(coordsSum[0],coordsSum[1],bins = 256 ,norm = LogNorm())
            plt.xlabel('x(m)')
            plt.ylabel('xp(rad)')

            plt.subplot(423)
            plt.hist2d(coordsSum[2],coordsSum[3],bins = 256, norm = LogNorm())
            plt.xlabel('y(m)')
            plt.ylabel('yp(rad)')

            plt.subplot(424)
            plt.hist2d(coordsSum[0],coordsSum[2],bins = 256, norm = LogNorm())
            plt.xlabel('x(m)')
            plt.ylabel('y(m)')

            plt.subplot(425)
            plt.hist(coordsSum[0], bins = 256)
            plt.yticks([])
            plt.xlabel('x(m)')

            plt.subplot(426)
            plt.hist(coordsSum[2], bins = 256)
            plt.yticks([])
            plt.xlabel('y(m)')

            plt.subplot(414)
            plt.hist(coordsSum[4], bins = 256)
            plt.yticks([])
            plt.xlabel('z(m)')

            plt.suptitle(str(turn)+'  turn',x = 0.05, y = 0.2, rotation = 90)
            plt.tight_layout()
            plt.savefig(filename)
            plt.close()

    def setPosition(self,pos):
        self.position = pos

    def setRange(self, start, end):
        self.start = start
        self.end = end

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength

    def setfilename(self,filename):
        self.filename = filename

class TeapotCounterNode(DriftTEAPOT):

    def __init__(self, name = "Counter"):
        """
        Constructor. Creates the counter TEAPOT element to count the tracking turns.
        """
        DriftTEAPOT.__init__(self,name)
        self.setType("Counter")
        self.setLength(0.0)
        self.lattlength = 0.0
        self.position = 0.0

    def track(self, paramsDict):
        """
        The counterNode class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        length = self.getLength(self.getActivePartIndex())
        bunch = paramsDict["bunch"]
        if bunch.hasBunchAttrInt('Turn'):
            turn = bunch.bunchAttrInt('Turn')+1
            bunch.bunchAttrInt("Turn",turn)
        else:
            bunch.bunchAttrInt("Turn",1)

    def setPosition(self,pos):
        self.position = pos

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength


class TeapotVersatileNode(DriftTEAPOT):

    def __init__(self, twissfile, TBTfile, BunchFile, interval, name = "Versatile no name"):
        """
        Constructor. Creates the versatile TEAPOT element.
        @twissfile: the filename for twiss parameters
        @TBTfile: the filename for bpm turn-by-turn data
        @BunchFile: the filename for detailed bunch information
        @interval: the interval to write out the data
        """
        DriftTEAPOT.__init__(self,name)
        self.tbtbpm = TeapotBPMTBTNode(TBTfile, name)
        self.stat = TeapotStatLatsNode(twissfile, name)
        self.tune = TeapotTuneAnalysisNode()
        self.bunchfile = BunchFile
        self.interval = interval
        self.setType("Versatile")
        self.lattlength = 0.0

    def track(self, paramsDict):
        """
        The versatile node class implementation of the AccNodeBunchTracker class track(probe) method.
        """
        bunch = paramsDict["Bunch"]
        turn = bunch.bunchAttrInt("Turn")
        if turn%self.interval == 0:
            self.stat.track(paramsDict)
            self.tbtbpm.track(paramsDict)
            self.tune.track(paramsDict)
            bunchfilename = str(turn)+"_"+self.bunchfile
            bunch.dumpBunch(bunchfilename)

    def setPosition(self,pos):
        self.tbtbpm.setPosition(pos)
        self.tune.setPosition(pos)
        self.stat.setPosition(pos)

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength
        self.tbtbpm.setLatticeLength(lattlength)
        self.tune.setLatticeLength(lattlength)
        self.stat.setLatticeLength(lattlength)

    def assignTwiss(self, betax, alphax, etax, etapx, betay, alphay):
        self.tune.assignTwiss(betax, alphax, etax, etapx, betay, alphay)

    def closeFile(self):
        self.tbtbpm.closeFile()
        self.stat.closeStatLats()

class TeapotTransActionNode(DriftTEAPOT):

    def __init__(self, lattice, InjectEnergy = 0.080, StartTime = 0,RatioOfPartList = [0.99, 0.95,], name = 'TransAction'):
        """
        Constructor. Creates a diagnostics node for TEAPOT Lattice to calculate the maximum trans action.
        The Trans action infos are  directly added to bunch attribute as double value.
        """
        DriftTEAPOT.__init__(self,name)
        self.setType("TransAction")
        self.lattlength = 0.0
        self.position = 0.0
        self.setLength(0.0)
        RatioOfPartList.sort(reverse = True)
        self.RatioOfPartList = RatioOfPartList
        self.lattice = lattice
        self.b_test = Bunch()
        self.b_test.getSyncParticle().time(StartTime)
        self.b_test.getSyncParticle().kinEnergy(InjectEnergy)
        self.matrix = TIME_DEP_MATRIX_Lattice(self.lattice, self.b_test)

    def track(self, paramsDict):
        """
        Call this method before dumpbunch.
        """
        comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
        rank = orbit_mpi.MPI_Comm_rank(comm)
        bunch = paramsDict["bunch"]
        syncP = bunch.getSyncParticle()
        t = syncP.time()
        beta = syncP.beta()
        Etot = syncP.mass() + syncP.kinEnergy()
        syn_test = self.b_test.getSyncParticle()
        syn_test.time(t)
        syn_test.kinEnergy(syncP.kinEnergy())
        self.matrix.rebuild(self.b_test)
        ring_par_dict = self.matrix.getRingParametersDict()
        betax = ring_par_dict["beta x [m]"]
        betay = ring_par_dict["beta y [m]"]
        alphax = ring_par_dict["alpha x"]
        alphay = ring_par_dict["alpha y"]
        etax = ring_par_dict["dispersion x [m]"]
        etapx = ring_par_dict["dispersion prime x"]
        index = 0
        macroPart = bunch.getSize()
        globalsize = bunch.getSizeGlobal()
        coords = [[], [], [], [], [], []]
        while index < macroPart:
            coords[0].append(bunch.x(index))
            coords[1].append(bunch.px(index))
            coords[2].append(bunch.y(index))
            coords[3].append(bunch.py(index))
            coords[4].append(bunch.z(index))
            coords[5].append(bunch.pz(index))
            index += 1
        coords_avg = [sum(a)/macroPart for a in coords]
        commsize = orbit_mpi.MPI_Comm_size(comm)
        op = orbit_mpi.mpi_op.MPI_SUM
        data_type = orbit_mpi.mpi_datatype.MPI_DOUBLE
        avg_sum = orbit_mpi.MPI_Allreduce(coords_avg, data_type, op, comm)
        avg_all = [a/commsize for a in avg_sum]

        index = 0
        xActionList = []
        yActionList = []
        while index < macroPart:
            x = bunch.x(index)
            xp = bunch.px(index)
            y = bunch.y(index)
            yp = bunch.py(index)
            dpp = 1/(beta*beta)*bunch.dE(index)/Etot
            xcanonical = x - etax * dpp - avg_all[0]
            ycanonical = y - avg_all[2]
            xpfac = xp - etapx * dpp
            ypfac = yp
            pxcanonical =  xpfac + xcanonical * (alphax/betax)
            pycanonical =  ypfac + ycanonical * (alphay/betay)
            xAction = xcanonical  *  xcanonical / betax + pxcanonical * pxcanonical * betax
            yAction = ycanonical  *  ycanonical / betay + pycanonical * pycanonical * betay
            xActionList.append(xAction)
            yActionList.append(yAction)
            index += 1

        RatioOfPartList = self.RatioOfPartList
        if RatioOfPartList[0] > 1 or RatioOfPartList[0] < 0:
            print 'Wrong range at TeapotTransActionNode! The RatioOfParticle should be between (0,1)'
            return None
        if rank != 0:
            orbit_mpi.MPI_Send(xActionList,
                    orbit_mpi.mpi_datatype.MPI_DOUBLE,0,99,comm)
            orbit_mpi.MPI_Send(yActionList,
                    orbit_mpi.mpi_datatype.MPI_DOUBLE,0,98,comm)
        else:
            xActionListSum = xActionList
            yActionListSum = yActionList
            for source in xrange(commsize-1):
                dx = orbit_mpi.MPI_Recv(orbit_mpi.mpi_datatype.MPI_DOUBLE,
                        source+1, 99, comm)
                xActionListSum = xActionListSum + list(dx)
                dy = orbit_mpi.MPI_Recv(orbit_mpi.mpi_datatype.MPI_DOUBLE,
                        source+1, 98, comm)
                yActionListSum = yActionListSum + list(dy)

            xActionListSum.sort()
            yActionListSum.sort()
            for RatioOfPartNow in RatioOfPartList:
                MaxActionIndex = int(globalsize*RatioOfPartNow)
                MaxXAction = xActionListSum[MaxActionIndex]
                MaxYAction = yActionListSum[MaxActionIndex]
                bunch.bunchAttrDouble("XMaxAction_" + str(RatioOfPartNow),MaxXAction)
                bunch.bunchAttrDouble("YMaxAction_" + str(RatioOfPartNow),MaxYAction)

    def setPosition(self,pos):
        self.position = pos

    def setLatticeLength(self, lattlength):
        self.lattlength = lattlength

    def setRatioOfParticle(self,RatioOfParticle):
        self.RatioOfParticle = RatioOfParticle

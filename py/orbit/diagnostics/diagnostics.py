#!/usr/bin/env python

"""
This is  a parallel version!
"""

# for mpi operations
import orbit_mpi
from orbit_mpi import mpi_comm
from orbit_mpi import mpi_datatype
from orbit_mpi import mpi_op

import math
import random
import sys
from bunch import BunchTwissAnalysis
from bunch import BunchTuneAnalysis
from orbit.utils.consts import speed_of_light

class StatLats:
        """
        This class gathers delivers the statistical twiss parameters
        """
        def __init__(self, filename):
                self.file_out = open(filename,"a")
                self.bunchtwissanalysis = BunchTwissAnalysis()
                self.columns = ['turn', 'time', 'emittance_x', 'emittance_y',
                        'Effective_emittance_x', 'Effective_emittance_y',
                        'Normalized_emittance_x', 'Normalized_emittance_y',
                        'Normalized_emittance z', 'beta_x (m)', 'beta_y (m)',
                        'alpha_x', 'alpha_y']
                self.title = 0

        def writeStatLats(self, s, bunch, lattlength = 0):
                self.bunchtwissanalysis.analyzeBunch(bunch)
                emitx = self.bunchtwissanalysis.getEmittance(0)
                betax = self.bunchtwissanalysis.getBeta(0)
                alphax = self.bunchtwissanalysis.getAlpha(0)
                betay = self.bunchtwissanalysis.getBeta(1)
                alphay = self.bunchtwissanalysis.getAlpha(1)
                emity = self.bunchtwissanalysis.getEmittance(1)
                emitfx = self.bunchtwissanalysis.getEffectiveEmittance(0)
                emitfy = self.bunchtwissanalysis.getEffectiveEmittance(1)
                emitnx = self.bunchtwissanalysis.getEmittanceNormalized(0)
                emitny = self.bunchtwissanalysis.getEmittanceNormalized(1)
                emitnz = self.bunchtwissanalysis.getEmittanceNormalized(2)
                names = bunch.bunchAttrDoubleNames()
                XMaxActions = [x for x in names if x.startswith('XMaxAction')]
                YMaxActions = [x for x in names if x.startswith('YMaxAction')]
                XMaxActions.sort(reverse=True)
                YMaxActions.sort(reverse=True)

                sp = bunch.getSyncParticle()
                time = sp.time()
                t = time
                if bunch.hasBunchAttrInt("Turn"):
                    turn = bunch.bunchAttrInt("Turn")
                else:
                    if lattlength > 0:
                        turn = sp.time()/(lattlength/(sp.beta() * speed_of_light))
                    else:
                        turn = 0

                comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
                rank = orbit_mpi.MPI_Comm_rank(comm)

                if (rank == 0):
                    values = [turn, t, emitx, emity, emitfx, emitfy, emitnx,
                            emitny, emitnz, betax, betay, alphax, alphay]
                    for c in zip(XMaxActions, YMaxActions):
                        if not self.title:
                            self.columns.append(c[0])
                            self.columns.append(c[1])
                        values.append(bunch.bunchAttrDouble(c[0]))
                        values.append(bunch.bunchAttrDouble(c[1]))
                    if not self.title:
                        columns ='#' +  '\t'.join(self.columns) + "\n"
                        self.file_out.write(columns)
                        self.title = 1
                    self.file_out.write('\t'.join(map(str, values)) + "\n")

        def closeStatLats(self):
                self.file_out.close()


class StatLatsSetMember:
        """
        This class delivers the statistical twiss parameters
        """
        def __init__(self, file):
                self.file_out = file
                self.bunchtwissanalysis = BunchTwissAnalysis()

        def writeStatLats(self, s, bunch, lattlength = 0):

                self.bunchtwissanalysis.analyzeBunch(bunch)
                emitx = self.bunchtwissanalysis.getEmittance(0)
                betax = self.bunchtwissanalysis.getBeta(0)
                alphax = self.bunchtwissanalysis.getAlpha(0)
                betay = self.bunchtwissanalysis.getBeta(1)
                alphay = self.bunchtwissanalysis.getAlpha(1)
                emity = self.bunchtwissanalysis.getEmittance(1)
                dispersionx = self.bunchtwissanalysis.getDispersion(0)
                ddispersionx = self.bunchtwissanalysis.getDispersionDerivative(0)
                #dispersiony = self.bunchtwissanalysis.getDispersion(1, bunch)
                #ddispersiony = self.bunchtwissanalysis.getDispersionDerivative(1, bunch)

                sp = bunch.getSyncParticle()
                time = sp.time()

                if lattlength > 0:
                        time = sp.time()/(lattlength/(sp.beta() * speed_of_light))

                # if mpi operations are enabled, this section of code will
                # determine the rank of the present node
                rank = 0  # default is primary node
                mpi_init = orbit_mpi.MPI_Initialized()
                comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
                if (mpi_init):
                        rank = orbit_mpi.MPI_Comm_rank(comm)

                # only the primary node needs to output the calculated information
                if (rank == 0):
                        self.file_out.write(str(s) + "\t" +  str(time) + "\t" + str(emitx)+ "\t" + str(emity)+ "\t" + str(betax)+ "\t" + str(betay)+ "\t" + str(alphax)+ "\t" + str(alphay) + "\t" + str(dispersionx) + "\t" + str(ddispersionx) +"\n")

        def closeStatLats(self):
                self.file_out.close()

        def resetFile(self, file):
                self.file_out = file



class Moments:
        """
                This class delivers the beam moments
        """
        def __init__(self, filename, order, nodispersion, emitnorm):
                self.file_out = open(filename,"a")
                self.bunchtwissanalysis = BunchTwissAnalysis()
                self.order = order
                if(nodispersion == False):
                        self.dispterm = -1
                else:
                        self.dispterm = 1

                if(emitnorm == True):
                        self.emitnormterm = 1
                else:
                        self.emitnormterm = -1

        def writeMoments(self, s, bunch, lattlength = 0):

                sp = bunch.getSyncParticle()
                time = sp.time()
                if lattlength > 0:
                        time = sp.time()/(lattlength/(sp.beta() * speed_of_light))

                self.bunchtwissanalysis.computeBunchMoments(bunch, self.order, self.dispterm, self.emitnormterm)

                # if mpi operations are enabled, this section of code will
                # determine the rank of the present node
                rank = 0  # default is primary node
                mpi_init = orbit_mpi.MPI_Initialized()
                comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
                if (mpi_init):
                        rank = orbit_mpi.MPI_Comm_rank(comm)

                # only the primary node needs to output the calculated information
                if (rank == 0):
                        self.file_out.write(str(s) + "\t" +  str(time) + "\t")
                        for i in range(0,self.order+1):
                                for j in range(0,i+1):
                                        self.file_out.write(str(self.bunchtwissanalysis.getBunchMoment(i-j,j)) + "\t")
                        self.file_out.write("\n")

        def closeMoments(self):
                self.file_out.close()



class MomentsSetMember:
        """
                This class delivers the beam moments
        """
        def __init__(self, file, order, nodispersion, emitnorm):
                self.file_out = file
                self.order = order
                self.bunchtwissanalysis = BunchTwissAnalysis()
                if(nodispersion == False):
                        self.dispterm = -1
                else:
                        self.dispterm = 1

                if(emitnorm == True):
                        self.emitnormterm = 1
                else:
                        self.emitnormterm = -1

        def writeMoments(self, s, bunch, lattlength = 0 ):

                sp = bunch.getSyncParticle()
                time = sp.time()

                if lattlength > 0:
                        time = sp.time()/(lattlength/(sp.beta() * speed_of_light))

                self.bunchtwissanalysis.computeBunchMoments(bunch, self.order, self.dispterm, self.emitnormterm)

                # if mpi operations are enabled, this section of code will
                # determine the rank of the present node
                rank = 0  # default is primary node
                mpi_init = orbit_mpi.MPI_Initialized()
                comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
                if (mpi_init):
                        rank = orbit_mpi.MPI_Comm_rank(comm)

                # only the primary node needs to output the calculated information
                if (rank == 0):
                        self.file_out.write(str(s) + "\t" +  str(time) + "\t")
                        for i in range(0,self.order+1):
                                for j in range(0,i+1):
                                        self.file_out.write(str(self.bunchtwissanalysis.getBunchMoment(i-j,j)) + "\t")
                        self.file_out.write("\n")

        def resetFile(self, file):
                self.file_out = file

class BPMSignal:
        """
                This class delivers the average value for coordinate x and y
        """
        def __init__(self):
                self.bunchtwissanalysis = BunchTwissAnalysis()
                self.xAvg = 0.0
                self.yAvg = 0.0
                self.xpAvg = 0.0
                self.ypAvg = 0.0

        def analyzeSignal(self, bunch):
                self.bunchtwissanalysis.analyzeBunch(bunch)
                # if mpi operations are enabled, this section of code will
                # determine the rank of the present node
                rank = 0  # default is primary node
                mpi_init = orbit_mpi.MPI_Initialized()
                comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
                if (mpi_init):
                        rank = orbit_mpi.MPI_Comm_rank(comm)

                # only the primary node needs to output the calculated information
                if (rank == 0):
                        self.xAvg = self.bunchtwissanalysis.getAverage(0)
                        self.xpAvg = self.bunchtwissanalysis.getAverage(1)
                        self.yAvg = self.bunchtwissanalysis.getAverage(2)
                        self.ypAvg = self.bunchtwissanalysis.getAverage(3)

        def getSignalX(self):
                return self.xAvg

        def getSignalXP(self):
                return self.xpAvg

        def getSignalY(self):
                return self.yAvg

        def getSignalYP(self):
                return self.ypAvg

class WCM:
        """
                This class get the 6-d coordinates of all the good particles from bunch
        """
        def __init__(self):
                self.coor = []

        def extractCoordinates(self, bunch):
                size = bunch.getSize()
                a=[]
                for i in xrange(size):
                    a.append(bunch.x(i))
                    a.append(bunch.px(i))
                    a.append(bunch.y(i))
                    a.append(bunch.py(i))
                    a.append(bunch.z(i))
                    a.append(bunch.pz(i))
                # if mpi operations are enabled, this section of code will
                # determine the rank of the present node
                comm = orbit_mpi.mpi_comm.MPI_COMM_WORLD
                rank = orbit_mpi.MPI_Comm_rank(comm)
                commsize = orbit_mpi.MPI_Comm_size(comm)
                # only the primary node needs to output the calculated information
                if commsize == 1:
                    self.setCoordinates(a)
                else:
                    if rank != 0:
                        orbit_mpi.MPI_Send(a,orbit_mpi.mpi_datatype.MPI_DOUBLE,0,99,comm)
                    else:
                        c = tuple(a)
                        for source in xrange(commsize-1):
                            d = orbit_mpi.MPI_Recv(orbit_mpi.mpi_datatype.MPI_DOUBLE,source+1,99,comm)
                            c = c+d #concatenate the tuple of coordinates
                        self.setCoordinates(c)

        def setCoordinates(self,a):
                size = len(a)/6
                x=[];xp=[];y=[];yp=[];z=[];zp=[]
                for j in xrange(size):
                    x.append(a[j*6+0])
                    xp.append(a[j*6+1])
                    y.append(a[j*6+2])
                    yp.append(a[j*6+3])
                    z.append(a[j*6+4])
                    zp.append(a[j*6+5])
                self.coor = [x,xp,y,yp,z,zp]

        def getCoordinates(self):
                return self.coor

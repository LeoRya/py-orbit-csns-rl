#ifndef Multi_Harmonic_Cav_H
#define Multi_Harmonic_Cav_H

//MPI Function Wrappers
#include "orbit_mpi.hh"
#include "wrap_mpi_comm.hh"

#include <cstdlib>
#include <cmath>

//ORBIT bunch
#include "Bunch.hh"

//pyORBIT utils
#include "CppPyWrapper.hh"

using namespace std;

class Multi_Harmonic_Cav: public OrbitUtils::CppPyWrapper
{
  public:
    Multi_Harmonic_Cav(double ZtoPhi, double dESync,
                       double *RFHarms, double *RFVoltages,
                       double *RFPhases, int nRFHarmonics);
    virtual ~Multi_Harmonic_Cav();
    void   setZtoPhi(double ZtoPhi);
    double getZtoPhi();
    void   setdESync(double dESync);
    double getdESync();
    int getnRFHarmonics();
    void   setRFVoltages(double RFVoltages[]);
    double* getRFVoltages();
    void   setRFPhases(double RFPhases[]);
    double* getRFPhases();
    void   setOldGamma(double Gamma);
    double getOldGamma();
    void   setOldBeta(double Beta);
    double getOldBeta();
    void   trackBunch(Bunch* bunch);

  private:
  double _ZtoPhi;
  double _dESync;
  double* _RFHarms;
  double* _RFVoltages;
  double* _RFPhases;
  int _nRFHarmonics;
  double _OldGamma;
  double _OldBeta;

  protected:

};

#endif
